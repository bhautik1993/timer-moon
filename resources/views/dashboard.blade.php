<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    Welcome to Dashboard.
                    <hr>
                    <br>
                    <button id="timerButton" type="button" class="btn {{($attendanceDB) ? 'btn-danger' : 'btn-success'}}">
                        {{($attendanceDB) ? 'Stop' : 'Start'}}</button>
                </div>

                <div class="p-6 row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>In/Out Time</th>
                                    <th>Break Taken</th>
                                    <th>Worked Hours</th>
                                    <th>Office Hours</th>
                                </tr>
                            </thead>
                            <tbody id="attendanceData">
                                
                                {{-- @foreach($responseArray as $key => $allAttendance)
                                    
                                    <tr>
                                        <td>{{$allAttendance['date']."". count($allAttendance['breakTimes']) }}</td>
                                        <td>
                                            <table class="table">
                                                @foreach($allAttendance['breakTimes'] as $breaksInOut)
                                                    <tr>
                                                            <td>{{$breaksInOut['in']}}</td>
                                                            <td>{{$breaksInOut['out']}}</td>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                        <td>
                                            {{Carbon\CarbonInterval::seconds($allAttendance['breaks'])->cascade()->forHumans()}}
                                        </td>
                                        <td>
                                            {{Carbon\CarbonInterval::seconds($allAttendance['officeTime'] - $allAttendance['breaks'])->cascade()->forHumans()}}
                                        </td>
                                        <td>
                                            {{Carbon\CarbonInterval::seconds($allAttendance['officeTime'])->cascade()->forHumans()}}
                                        </td>
                                    </tr>
                                
                                @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script>
        $(document).ready(function(){
            displayAttendanceData();
            $(document).on("click","#timerButton",function() {
                $.ajax({
                    url: "{{route('attendance.store')}}", 
                    type: "GET",
                    success: function(result){
                        if(result.start){
                            $("#timerButton").html('Start');
                            $("#timerButton").removeClass('btn-danger').addClass('btn-success');
                        }else{
                            $("#timerButton").html('Stop');
                            $("#timerButton").removeClass('btn-success').addClass('btn-danger');
                            displayAttendanceData();
                        }
                        //$("#response").html(result);
                    },
                    error: function(result){
                        alert("Something wend wrong");
                    }
                });
            });
            
        });

        function displayAttendanceData(){
            $.ajax({
                url: "{{route('attendance.get')}}", 
                type: "GET",
                success: function(result){
                    $("#attendanceData").html(result);
                },
                error: function(result){
                    alert("Something wend wrong");
                }
            });
        }
    </script>
</x-app-layout>
