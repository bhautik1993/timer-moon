
@foreach($responseArray as $key => $allAttendance)
    
    <tr>
        <td>{{$allAttendance['date']."". count($allAttendance['breakTimes']) }}</td>
        <td>
            <table class="table">
                @foreach($allAttendance['breakTimes'] as $breaksInOut)
                    <tr>
                            <td>{{$breaksInOut['in']}}</td>
                            <td>{{$breaksInOut['out']}}</td>
                        </td>
                    </tr>
                @endforeach
            </table>
        </td>
       
        <td>
            {{Carbon\CarbonInterval::seconds($allAttendance['officeTime'] - $allAttendance['breaks'])->cascade()->forHumans()}}
        </td>
        <td>
            {{Carbon\CarbonInterval::seconds($allAttendance['breaks'])->cascade()->forHumans()}}
        </td>
        <td>
            {{Carbon\CarbonInterval::seconds($allAttendance['officeTime'])->cascade()->forHumans()}}
        </td>
    </tr>

@endforeach