<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;

class AttendenceController extends Controller
{
    public function index(){
        $attendanceDB = Attendance::where('user_id',Auth::user()->id)
        ->whereDate('checked_in', Carbon::now()->format('Y-m-d'))
        ->whereNull('checked_out')
        ->first();

        $allAttendanceDB = Attendance::where('user_id',Auth::user()->id)
        ->groupBy(DB::raw("DATE_FORMAT(checked_in, '%Y-%d-%d')"))
        ->get();

        $responseArray = [];
        foreach($allAttendanceDB as $allAttendance){
            $allAttendanceDetailDB = Attendance::where('user_id',Auth::user()->id)
                ->whereDate('checked_in', Carbon::parse($allAttendance->checked_in)->format('Y-m-d'))
                ->get();
                $breakTimes = [];
                $breakTime = 0;
                $officeTime = 0;
                $workedTime = 0;
                foreach ($allAttendanceDetailDB as $key => $allAttendanceDetail) {
                    
                    if($key==0){
                        $firstTime = Carbon::parse($allAttendanceDetail->checked_in);
                    }
                    if($allAttendanceDetailDB->count() == $key+1){
                        $lastTime = Carbon::parse($allAttendanceDetail->checked_out);
                        $officeTime = $lastTime->diffInSeconds($firstTime);
                    }

                    $startTime = Carbon::parse($allAttendanceDetail->checked_in);
                    $endTime = Carbon::parse($allAttendanceDetail->checked_out);
                    $breakTime += $endTime->diffInSeconds($startTime);
                    array_push($breakTimes, [
                        'in'=>Carbon::parse($allAttendanceDetail->checked_in)->format('Y-m-d'),
                        'out'=>Carbon::parse($allAttendanceDetail->checked_out)->format('Y-m-d'),
                    ]);    
                }
                array_push($responseArray, ['date'=>Carbon::parse($allAttendance->checked_in)->format('Y-m-d'),'breakTimes'=>$breakTimes,'breaks'=>$breakTime,'officeTime'=>$officeTime]);
        }
        //  dd($responseArray);
        return view('dashboard', compact('attendanceDB','responseArray'));
    }

    public function store(){
        $attendanceDB = Attendance::where('user_id',Auth::user()->id)
        ->whereDate('checked_in', Carbon::now()->format('Y-m-d'))
        ->whereNull('checked_out')
        ->first();
        if($attendanceDB){
            $attendanceDB->checked_out = Carbon::now();
            $attendanceDB->save();
            return response()->json(['message'=> 'Stopped', 'start'=>1]);
        }else{
            $attendanceDB = new Attendance();
            $attendanceDB->checked_in = Carbon::now();
            $attendanceDB->user_id = Auth::user()->id;
            $attendanceDB->save();
            return response()->json(['message'=> 'Started', 'start'=>0]);
        }
    }

    public function getAllattendance(){
        $allAttendanceDB = Attendance::where('user_id',Auth::user()->id)
        ->groupBy(DB::raw("DATE_FORMAT(checked_in, '%Y-%d-%d')"))
        ->get();

        $responseArray = [];
        foreach($allAttendanceDB as $allAttendance){
            $allAttendanceDetailDB = Attendance::where('user_id',Auth::user()->id)
                ->whereDate('checked_in', Carbon::parse($allAttendance->checked_in)->format('Y-m-d'))
                ->get();
                $breakTimes = [];
                $breakTime = 0;
                $officeTime = 0;
                $workedTime = 0;
                foreach ($allAttendanceDetailDB as $key => $allAttendanceDetail) {
                    
                    if($key==0){
                        $firstTime = Carbon::parse($allAttendanceDetail->checked_in);
                    }
                    if($allAttendanceDetailDB->count() == $key+1){
                        $lastTime = Carbon::parse($allAttendanceDetail->checked_out);
                        $officeTime = $lastTime->diffInSeconds($firstTime);
                    }

                    $startTime = Carbon::parse($allAttendanceDetail->checked_in);
                    $endTime = Carbon::parse($allAttendanceDetail->checked_out);
                    $breakTime += $endTime->diffInSeconds($startTime);
                    array_push($breakTimes, [
                        'in'=>Carbon::parse($allAttendanceDetail->checked_in)->format('h:i:s A'),
                        'out'=>Carbon::parse($allAttendanceDetail->checked_out)->format('h:i:s A'),
                    ]);    
                }
                array_push($responseArray, ['date'=>Carbon::parse($allAttendance->checked_in)->format('Y-m-d'),'breakTimes'=>$breakTimes,'breaks'=>$breakTime,'officeTime'=>$officeTime]);
        }
        return view('dashboardAttendanceTable', compact('responseArray'));
    }
}
