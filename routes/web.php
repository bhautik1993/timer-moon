<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Admin\AttendenceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard'); */

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/dashboard', [AttendenceController::class, 'index'])->name('dashboard');
    Route::get('attendance/store', [AttendenceController::class, 'store'])->name('attendance.store');
    Route::get('attendance', [AttendenceController::class, 'getAllattendance'])->name('attendance.get');
});

require __DIR__.'/auth.php';
